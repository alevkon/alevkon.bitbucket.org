var Calculator = Backbone.Model.extend({

    defaults: {
        remain: 25000,
        period: null,
        rate: null,
        payment: null,
        status: "empty",
        queue: ["remain"],
        disabled: null,
        mode: "1"
    },

    initialize: function() {
        var self = this;
        Backbone.Model.prototype.initialize.apply(this, arguments);
        new CalculatorView({
            model: this
        });
        this.on({
            "change:remain change:period change:rate change:payment": function() {
                if (self.prevent) return;
                for (var i in self.changed) {
                    if (self.changed[i] != null) {
                        self.registerChange(i);
                    }
                    break;
                }
                self.calculate();
            },
//            "change:disabled": function(model, value) {
//                if (value) return;
//                self.registerChange(self.previous("disabled"), true);
//                var nullField = self.getNullField(self.previous("disabled"));
//
//                if (nullField) {
//                    self.set("disabled", nullField);
//                }
//                self.detectStatus();
//            }
        });
    },

    formatSum: function(value) {
        var reverse = String(value).replace(/\s/g, "").split("").reverse().join("")
            , spaced = reverse.replace(/([0-9]{3})/g, "$1 ")
            , fixed = spaced.split("").reverse().join("")
            , trimmed = fixed.replace(/^\s+/, "");

        return trimmed;
    },

    getNullField: function(exclude) {
        var self = this;
        return _.find(["remain", "period", "rate", "payment"], function(item) {
            return null == self.get(item) && (!exclude || item != exclude);
        });
    },

    registerChange: function(key, forceEvenIfNull) {
        if (null == this.get(key) && !forceEvenIfNull) return;

        var filtered = _.without(this.get("queue"), key);

        filtered.push(key);
        if (filtered.length > 3) {
            this.prevent = true;
            this.set(filtered[0], null);
            this.prevent = false;
            filtered.splice(0, filtered.length - 3);
        }
        this.set("queue", filtered);
    },

    prepareData: function() {
        var nullField = this.getNullField()
            , data = _.pick(this.attributes, "remain", "period", "rate", "payment");
        switch(nullField) {
            case "payment":
                data.payment = - Formula.PMT(0.01 * data.rate/12, data.period, data.remain, 0, 0);
                break;
            case "rate":
                data.rate = Math.abs(100 * Formula.RATE(data.period, -data.payment, data.remain) * 12);
                break;
            case "period":
                data.period = Formula.NPER(0.01 * data.rate/12, -data.payment, data.remain);
                break;
            case "remain":
                data.remain = Formula.PV(0.01 * data.rate/12, data.period, -data.payment, 0, 0);
                break;
        }
        return data;
    },

    detectStatus: function() {
        var counter = 0
            , nullField = null
            , keys = ["remain", "period", "rate", "payment"];

        for (var i=0; i<keys.length; i++) {
            if (this.get(keys[i]) != null) {
                counter++;
            } else {
                nullField = keys[i];
            }
        }
        if (counter < 3) return "empty";
        if (nullField) {
            this.set("disabled", nullField);
        }

        return "ready";
    },

    calculate: function() {
        this.set("status", this.detectStatus());
        if ("empty" == this.get("status")) return;

        var data = this.prepareData()
            , nullField = this.getNullField();

        var offer = {
             mode1: {
                rate: 12.9,
                payment: -Formula.PMT(0.01*12.9/12, data.period, data.remain, 0, 0)
            },
            mode2: {
                rate: 12.9,
                period: 60,
                payment: -Formula.PMT(0.01*12.9/12, 60, data.remain, 0, 0)
            },
            fixGap: {
            }
        };
        offer.fixGap[nullField] = data[nullField];

        try {
            if (isNaN(offer.mode1.payment) || isNaN(offer.mode2.payment)) throw new Error();
            if (offer.mode1.payment <= 0 || offer.mode2.payment <= 0) throw new Error();

//            if (data.rate <= 12.9) throw new Error();
            if (data.remain > 800000) throw new Error();

            offer.mode1.benefit = (data.payment - offer.mode1.payment) * data.period;
            if (offer.mode1.benefit < 0) throw new Error();

            this.set("offer", offer);
        } catch(e) {
            this.set("status", "wrong");
        }
    }
});

var CalculatorView = Backbone.View.extend({
    el: $("#calculator"),

    events: {
        "click .calculator__close": function(event) {
            event.preventDefault();
            this.model.set({
                remain:25000,
                period: null,
                payment: null,
                rate: null,
                status: "empty"
            });
        },
        "click .tabs__item a": function(event) {
            event.preventDefault();
            this.model.set("mode", $(event.target).attr("data-mode"));
        },
        "mousedown input": function(event) {
            this.enableInput($(event.target));
        },
        "keypress input": function(event) {
            if (13 == event.keyCode) {
                this.collectOneInput($(event.target))
            }
        },
        "blur input": function(event) {
            this.collectOneInput($(event.target))
        }
    },

    enableInput: function(input) {
        if (input.attr("disabled") == "disabled") {
            this.model.set("disabled", false);
            input.removeAttr("disabled");
        }
        if ("remain" == input.attr("name")) {
            this.$(".remain-slider").removeClass("disabled");
        }
    },

    collectOneInput: function(input) {
        var raw = input.val()
            , localized = raw.replace(/\,/g, ".")
            , trimmed = localized.replace(/\s/g, "")
            , value = trimmed.length ? Number(trimmed) : null;

        input.addClass("locked");
        if (null == value || isNaN(value)) {
            value = null;
            input.val("");
        } else {
            switch(input.attr("name")) {
                case "remain":
                    value = Math.min(value, 800000);
                    break;
            }
            value = Math.max(value, 0);
            input.val(value);
            this.formatInput(input);
        }
        this.model.set(input.attr("name"), value);
        input.removeClass("locked");
    },

    formatInput: function(input) {
        if (!input.hasClass("format")) return;
        input.val(this.model.formatSum(input.val()));
    },

    renderStatus: function() {
        this.$el
            .removeClass("calculator_status_ready calculator_status_empty calculator_status_wrong")
            .addClass("calculator_status_" + this.model.get("status"));
    },


    renderMode: function() {
        this.$el
            .removeClass("calculator_mode_1 calculator_mode_2")
            .addClass("calculator_mode_" + this.model.get("mode"));
    },

    initialize: function() {
        var self = this;
        Backbone.View.prototype.initialize.apply(this, arguments);

        this.$("input[name='remain']").val(this.model.get("remain"));
        this.formatInput(this.$("input[name='remain']"));
        this.renderStatus();
        this.renderMode();

        this.$(".remain-slider").slider({
            min: 25000,
            max: 800000,
            step: 1000,
            animate: "fast",
			range: "min",
            value: this.model.get("remain"),
            slide: function(event, ui) {
                self.model.set("remain", ui.value);
                self.enableInput(self.$("input[name=remain]"));
            }
        });

        this.model.on({
            "change:mode": function(model) {
                self.renderMode();
            },
            "change:offer": function(model, offer) {
                self.$(".calculator__offer_1 .calculator__offer__rate").text(offer.mode1.rate);
                self.$(".calculator__offer_1 .calculator__offer__payment").text(Math.round(offer.mode1.payment));
                self.$(".calculator__offer_1 .calculator__offer__benefit").text(self.model.formatSum(Math.floor(offer.mode1.benefit/100)*100));

                self.$(".calculator__offer_2 .calculator__offer__rate").text(offer.mode2.rate);
                self.$(".calculator__offer_2 .calculator__offer__period").text(Math.round(offer.mode2.period));
                self.$(".calculator__offer_2 .calculator__offer__payment").text(self.model.formatSum(Math.floor(offer.mode2.payment/100)*100));

                for (var i in offer.fixGap) {
                    var value = offer.fixGap[i];
                    switch(i) {
                        case "payment":
                            value = self.model.formatSum(Math.round(value));
                            break;
                        case "rate":
                            value = Math.round(value * 100)/100;
                            break;
                        default:
                            value = Math.round(value);
                            break;
                    }
                    self.$("input[name=" + i + "]").val(value);
                }
            },

            "change:disabled": function(model, value) {
                if (value) {
                    self.$("input[name=" + value + "]").attr("disabled", "disabled");
                    if ("remain" == value) {
                        self.$(".remain-slider").addClass("disabled");
                    }
                }
            },
            "change:status": function(model) {
                self.renderStatus();
            },
            "change:remain change:period change:rate change:payment": function(model) {
                for (var i in model.changed) {
                    if ("remain" == i) {
                        self.$(".remain-slider").slider({
                            value: model.changed[i]
                        });
                    }
                    var input = self.$("input[name='" + i + "']");
                    if (!input.hasClass("locked")) {
                        input.val(model.changed[i]);
                        self.formatInput(input);
                    }
                }
            }
        });
    }
});