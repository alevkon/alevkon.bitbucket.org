var App = Backbone.Model.extend({
    defaults: {
        introMessages: [
        ],
        scrollStack: []
    },

    initialize: function() {
        Backbone.Model.prototype.initialize.apply(this, arguments);

        this.set("scroll", new Scroll());
        this.set("calculator", new Calculator());
    },
    sendSMS: function() {
        alert('Implement SMS sending');
        window.isSmsSent = true;
    },
    checkSMS: function() {
        var data = {
            code: $("input[name=sms_code]").val(),
            phone: $("input[name=phone]").val()
        };
        if (!data.code || !data.code.length) return;

        //todo: ajax this data
        setTimeout(function() {
            $("input[name=sms_code]")
                .addClass("valid")
                .removeClass("wrong")
                .attr("disabled", "disabled");
            $("input[name=phone]").attr("disabled", "disabled");
        }, 1000);
    }
});

var AppView = Backbone.View.extend({
    el: $("#wrapper"),
    events: {
        "click .scroll-to": function(event) {
            event.preventDefault();
            var target = $(event.target).attr("data-target");
            if (target && $(target).length) {
                $("html, body").animate({
                    scrollTop: $(target).offset().top + "px"
                });
            }
        },
        "blur input[name=sms_code]": function(event) {
            this.model.checkSMS();
        },
        "keydown input[name=sms_code]": function(event) {
            if (13 == event.keyCode) {
                this.model.checkSMS();
            }
        },
        "click .send-sms": function(event) {
            event.preventDefault();

            this.model.sendSMS();
        },
//        "click .intro__again": function(event) {
//
//            event.preventDefault();
//            var message = this.model.get("introMessages")[Math.floor(Math.random() * this.model.get("introMessages").length)];
//            this.$(".intro__message__text").text(message);
//        },
        "click .calculator__go": function(event) {
            var self = this;
            this.$(".calculator__go").attr("disabled", "disabled");
            this.$(".slide.anketa").animate({
                height: $(window).height() + "px"
            }, {
                complete: function() {
                    self.$(".slide.snap-bottom").removeClass("snap-bottom");
                    self.$(".slide.anketa")
                        .addClass("snap-bottom")
                        .css("height", "auto");
                }
            });
            $("html, body").animate({
                scrollTop: this.$(".slide.anketa").offset().top + "px"
            });
        },
        "click .anketa__thanks": function(event) {
            var self = this;
            this.$(".calculator__go").removeAttr("disabled");
            this.$(".slide.anketa").animate({
                height: "0px"
            }, {
                complete: function() {
                    self.$(".anketa")
                        .removeClass("anketa_mode_done")
                        .addClass("anketa_mode_form");

                    self.$(".slide.snap-bottom").removeClass("snap-bottom");
                    self.$("#calculator").addClass("snap-bottom");
                }
            });
            $("html, body").animate({
                scrollTop: $("#calculator").offset().top + "px"
            });

            self.$(".anketa input[type=checkbox]").removeAttr("checked");
            self.$(".anketa input[type=text]")
                .val("")
                .removeAttr("disabled");
            self.$(".anketa .form__icon-ok").remove();
        }
    },

    submitForm: function(form) {
        var self = this
            , data = {};

        if (!$("input[name=sms_code]").hasClass("valid")) {

            $("input[name=sms_code]")
                .addClass("wrong")
                .focus();

            return;
        }

        this.$(".data-source").each(function(i, dataSource) {
            var $dataSource = $(dataSource);

            if ($dataSource.attr("name")) {
                var val = "checkbox" == $dataSource.attr("type")
                    ? $dataSource.is(":checked")
                    : $dataSource.val()

                data[$dataSource.attr("name")] = val;
            }
        });

        alert(JSON.stringify(data));

        this.$(".anketa__done")
            .css({
                display: "block",
                height: "0px"
            })
            .animate({
                height: "240px"
            });

        this.$(".anketa__form")
            .animate({
                height: "0"
            }, {
                complete: function() {
                    self.$(".anketa")
                        .addClass("anketa_mode_done")
                        .removeClass("anketa_mode_form");

                    self.$(".anketa__done")
                        .css({
                            "height":"auto",
                            "display": ""
                        });
                    self.$(".anketa__form").css("height", "auto");
                }
            });

//        $("html, body").animate({
//            scrollTop: $(".anketa").offset().top + "px"
//        });
    }
});

$(function() {
    window.gAppView = new AppView({
        model: window.gApp = new App()
    }).render();
});