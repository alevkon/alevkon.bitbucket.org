var Scroll = Backbone.Model.extend({

    SNAP_DISTANCE: 300,
    SNAP_DURATION: 300,
    SCROLL_TIMEOUT: 500,

    defaults: {
        introMessages: [
            "Банк Открытие —\nэто хорошо",
            "Другие банки —\nэто некруто",
            "Варкалось.\nХливкие шорьки\nпырялись по наве."
        ],
        scrollStack: [],
        scrollTimer: null
    },
    initialize: function() {
        Backbone.Model.prototype.initialize.apply(this, arguments);
        new ScrollView({
            model: this
        })
    }
});

var ScrollView = Backbone.View.extend({
    el: $("#wrapper"),
    checkScroll: function(reason) {
        var self = this
            , stack = this.model.get("scrollStack");

        if ($("html, body").is(":animated")) {
            if ("native" == reason) return;

            $("html, body").stop();
            this.resetScroll();
        } else {
            if ("native" != reason) return;
        }

        if (stack.length && stack[stack.length - 1] == $(window).scrollTop()) return;

        stack.push($(window).scrollTop());
        if (stack.length > 10) {
            stack.splice(0, stack.length - 10);
        }

        var scrollTimer = this.model.get("scrollTimer");
        if (scrollTimer) {
            clearTimeout(scrollTimer);
        }
        this.model.set("scrollTimer", setTimeout(function() {
            self.model.trigger("scroll-stop");
        }, Scroll.prototype.SCROLL_TIMEOUT));
    },

    resizeSlides: function () {
        $(".slide").css({
            position: "relative",
            minHeight: Math.max(540, $(window).height()) + "px",
            top: ""
        });
        $("#wrapper").css({
            position: "relative"
        });
        $(".slide.no-fix").css({
            minHeight: ""
        });
		$('.benefits').each(function(){
			var h1 = $(this).parents('.slide').height(),
				h2 = $(this).parents('.content').height();
			
			$(this).parents('.content')
				.css({
					'left': Math.max($(window).width()/2 - $(this).parents('.content').width()/2, 0) + 'px',
					'top': Math.max($(this).parents('.slide').height()/2 - $(this).parents('.content').height()/2, 0) + 'px',
					'position': 'absolute'
				})
		})
    },

    initialize: function() {
        var self = this;
        Backbone.View.prototype.initialize.apply(this, arguments);

        this.resizeSlides();
        $(window).resize(function() {
            self.resizeSlides();
        });

        $(window).scroll(function() {
            self.checkScroll("native");
        });
        $(window).mousewheel(function() {
            self.checkScroll("mousewheel");
        });
        $(document).keydown(function(event) {
            switch(event.keyCode) {
                case 33: //page up
                case 34: //page down
                case 35: //end
                case 36: //home
                case 38: //up
                case 40: //down
                    self.checkScroll("key");
                    break;
            }
        });

        this.model.on({
            "scroll-stop": function() {
                var stack = self.model.get("scrollStack");
                if (stack.length <= 1) return;

//                //ensure 10 last scrolls was in same direction
//                var dist = stack[1] - stack[0];
//                for (var i=1; i<stack.length; i++) {
//                    if ((stack[i] - stack[i - 1]) * dist < 0) {
//                        self.resetScroll();
//                        return;
//                    }
//                }

                //detect closest snap
                var snap = self.getClosestSnap(stack[stack.length - 1]);
                if (snap && Math.abs(snap.distance) < Scroll.prototype.SNAP_DISTANCE) {
                    $("html, body").animate({
                        scrollTop: snap.slide.offset().top// + $("#wrapper").scrollTop()
                    }, {
                        duration: Scroll.prototype.SNAP_DURATION,
                        complete: function() {
                            self.resetScroll();
                        }
                    });
                }
            }
        });
    },

    resetScroll: function() {
        this.model.set("scrollStack", []);
    },

    getClosestSnap: function(scrollTop) {
        var closest = null,
            closestDistance = null;

        if (scrollTop > $(".snap-bottom").offset().top) return null;

        this.$(".slide").each(function(i, slide) {
            var distance = ($(slide).offset().top ) - scrollTop;
            if (Math.abs(distance) < Math.abs(closestDistance) || null == closest) {
                closest = $(slide);
                closestDistance = distance;
            }
        });
        return {
            slide: closest,
            distance: closestDistance
        };
    }
});