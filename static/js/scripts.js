﻿$(function(){
	var $form = getCurrentForm();
	initFormValidation($form);
	initFormSelect($form);
	initFormCheckbox($form);
	initFormMask($form);
	initCustomScroll();
	initSmsField($form);
})

function getCurrentForm(){
	if($('#application_form').length > 0)
		return $('#application_form');
	else
		return $('#test_form');
}

function initFormValidation($form){
	$form.validate({
		ignore: '',
        rules:{
            second_name: {
				required: true,
				russian: true,
				minlength: 3
			},
            last_name: {
				required: true,
				russian: true,
				minlength: 3
			},
			name: {
				required: true,
				russian: true,
				minlength: 3
			},
			age: {
				required: true,
				digits: true,
				min: 21
			},
			exp: 'required',
			email: {
				required: true,
				email: true
			},
			agreement: 'required',
			phone: 'required',
//			sms_code: 'has-valid-class',
			region_reg: 'required',
			region_work: 'required',
			emp_type: 'required',
			experience: 'required'
        },
		messages: {
			email: {
				email: "Введите корректный адрес электронной почты"
			},
			age: {
				min: "Вам должно быть больше чем 21 год"
			}
		},
		submitHandler: function(form) {
			$('.slide_4 .red-alert').hide();
            gAppView.submitForm(form);
            return false;
		},
        invalidHandler: function(form){
			$('.slide_4 .red-alert').show();
            $("html, body").animate({
                scrollTop: $(".slide.anketa").offset().top
            });
        },
        errorPlacement: function(error, element) {
			var $field_wrap = $(element).parents('.form__field'),
				$field_icon = $field_wrap.find('.form__icon'),
				$field_hint = $field_wrap.find('.form__hint-error');
            if($(element).attr('type')=='text' || $(element).is('textarea')){
                
				if($field_icon.length==0) {
					var iconHTML = '<div class="form__icon form__icon-ok hidden"></div>';
					$field_wrap.append(iconHTML);
				}
            }
			if($field_hint.length==0) {
				var hintHTML = '<div class="form__hint form__hint-error serif" style="display:none">'+
									'<div class="form__hint-title"></div>'+
									'<div class="form__hint-background"></div>'+
									'<div class="form__hint-arrow"></div>'+
								'</div>';
				$field_wrap.append(hintHTML);
			}
			$field_wrap.find('.form__hint-error .form__hint-title').text($(error).text());
        },
        highlight: function(element, errorClass, validClass) {
			 var $field_wrap = $(element).parents('.form__field');
				 $field_icon = $field_wrap.find('.form__icon');
			
			$field_wrap.addClass('form__field-error');
			$(element).addClass("wrong");
			if($(element).is(':text') || $(element).is('textarea')){
				$field_icon.addClass('form__icon-error').removeClass('form__icon-ok').hide();
			}
			if($(element).attr('type')=='text' || $(element).is('textarea')){
				$field_wrap.addClass('form__field-error');
			}
			else if($(element).is('select')){
				$field_wrap.find('.bootstrap-select').addClass('bootstrap-select-errored');
			}
			else if($(element).attr('type')=='checkbox'){
				$field_wrap.addClass('form__field-checkbox-error');
			}
		},
        unhighlight: function(element, errorClass, validClass) {
			 var $field_wrap = $(element).parents('.form__field'),
				 $field_hint = $field_wrap.find('.form__hint-error');
				 $field_icon = $field_wrap.find('.form__icon');
			
			$field_hint.hide();
			$field_wrap.removeClass('form__field-error');
			$(element).removeClass("wrong");
            if($(element).is(':text') || $(element).is('textarea')){
				$field_icon.removeClass('form__icon-error').addClass('form__icon-ok').show();
			}
			if($(element).attr('type')=='text' || $(element).is('textarea')){
				$field_wrap.removeClass('form__field-error');
			}
			else if($(element).is('select')){
				$field_wrap.find('.bootstrap-select').removeClass('bootstrap-select-errored');
			}
			else if($(element).attr('type')=='checkbox'){
				$field_wrap.removeClass('form__field-checkbox-error');
			}
        },
		success: function(label) {}
    });
	
	/*$form.bind('change keyup', function() {
		$form.find('[type="submit"]').attr('disabled', !$(this).validate().checkForm());
	});*/
	
	$form.find('select').bind("change", function(){
//        alert("select changed: " + $(this).val());
		$form.validate().element(this);
	});
	
	$form.find('.form__field')
		.on('mouseenter', function(){
			if($(this).hasClass('form__field-error')){
				$(this).find('.form__hint-error').show();
			}
		})
		.on('mouseleave', function(){
			$(this).find('.form__hint-error').hide();
		})
}

function initFormMask($form){
	$form.find('[name="phone"]').mask("+7 ( 999 ) 999 - 99 - 99",{placeholder:" "});
}

function initFormSelect($form){
	 $form.find('select').selectpicker({
		container: 'body'
	 });
}

function initFormCheckbox($form){
	$form.find('.form__field-checkbox')
		.on('mouseenter', function(){
			$(this).addClass('form__field-checkbox-hovered');
		})
		.on('mouseleave', function(){
			$(this).removeClass('form__field-checkbox-hovered');
		})
}

function initCustomScroll(){
	$('#agreement_text').jScrollPane();
}

function initSmsField($form){
	window.isSmsSent = false;
	$form.find('input[name="phone"]').bind("change keyup", function(){
		var value = $(this).val(),
			arNumbers = value.match(/\d+/g);
		if(!window.isSmsSent && arNumbers && arNumbers.length > 0 && arNumbers.join("").length==11){
			sendSMS($form);
		}
	})
}

function sendSMS($form){
	gApp.sendSMS();
	$form
		.find('input[name="sms_code"]').removeAttr("disabled").focus()
		.next().show();
}